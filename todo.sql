

create database todos;


create table user ( 
    id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    email varchar(100) UNIQUE, 
    pseudo varchar(30) UNIQUE, 
    password varchar(255) 
);


create table todo (
     id_todo int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
    titre varchar(255), 
    description text, 
    cree_le date, 
    date_limite date , 
    status varchar(50), 
    categorie varchar(50) 
);

ALTER TABLE todo
ADD column id_user int;

ALTER TABLE todo
ADD FOREIGN KEY (id_user) REFERENCES user(id);

CREATE USER 'todo'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON todos . * TO 'todo'@'localhost';

-- Donnée de test 

-- Créer 6 utilisateurs car dans les lignes d'après on met id_user 5 et 6 (sinon, chnger les id_user dans la ligne d'après)

INSERT INTO todo (titre, description, cree_le, date_limite, status, categorie, id_user) 
VALUES ('Mettre la table', 'ne pas oublier les couverts', current_date(), '2022-09-02', 'À faire', 'tâche ménagère', 5), 
('Passer la serpillère', 'Bien frotter le sol, attention au parquet', current_date(), '2022-09-02', 'À faire', 'tâche ménagère', 6); 

select* FROM user WHERE id=8;

select* FROM todo;

select* FROM user;