<?php

require_once "bd.php";

// démarrer la session
session_start();

//verifier si utilisateur es connecter sinon redirection
if(!isset($_SESSION['idUser'])){
    //redirection vers la page connexion
    header('Location: ../connexion.php');
}






// récupérer l'id de l'utilisateur connecté
if(isset($_SESSION['idUser'])){

    if(isset($_POST['submit']))
{   
    //Nettoyage/sécurisation des données
   $titre =htmlspecialchars (trim($_POST['titre']));
   $description =htmlspecialchars (trim($_POST['description']));
   $cree_le=$_POST['cree_le'];
   $date_limite =$_POST['date_limite'];
   $status =htmlspecialchars (trim($_POST['status']));// il sera aprés une option
   $categorie =htmlspecialchars (trim($_POST['categorie']));
   $idUser = $_SESSION['idUser'];
    // insérer des données ,
   $requeteSQL = "INSERT INTO todo (titre,description,cree_le,date_limite,status,categorie, id_user) VALUES (:titre, :description, :cree_le, :date_limite, :status, :categorie, :id_value)";
    // reparer la requête
   $requetePreparee = $db->prepare($requeteSQL);
   $data = [
    ':titre'=> $titre, 
    ':description'=> $description,
    ":cree_le" => $cree_le,
    ":date_limite" => $date_limite,
    ':status' => $status,
    ':categorie' => $categorie,
    ':id_value' => $idUser
   ];
   //$requetePreparee->execute($data);
  if ($requetePreparee->execute($data)) {
    header('Location: ../todo.php');
  }
}

}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style.css">
    
</head>

<body>
    <form action=""  method="post">
        <div class="field-container">
            <label for="titre">Titre: <span class="required"></span></label>
            <input type="text" id="titre" name="titre" placeholder=" titre" required="required" />
        </div>

        <div class="field-container">
            <label for="description">Description: <span class="required"></span></label>
            <input type="text" id="description" name="description" placeholder="description" required="required" />
        </div>

        <div class="field-container">
            <label for="date">Cree_le: <span class="required"></span></label>
            <input type="date" id="date" name="cree_le"  required="required" />
        </div>

        <div class="field-container">
            <label for="date_limite">Date_limite: <span class="required"></span></label>
            <input type="date" id="date_limit" name="date_limite" placeholder="date_limite" required="required" />
        </div>

        <div class="field-container">
            <label for="status">Status: <span class="required"></span></label>
            <select name="status">
                <option value="A faire">A faire</option>
                <option value="En cours">En cours</option>
                <option value="Fait">Fait</option>
            </select>
            <!---<input type="text" id="status" name="status" placeholder="status" required="required" />-->
        </div>

        <div class="field-container">
            <label for="categorie">Categorie: <span class="required"></span></label>
            <input type="text" id="categorie" name="categorie" placeholder="categorie" required="required" />
        </div>
        <div class="center"><input type="submit" name="submit" value="Ajouter"></div>


    </form>
</body>

</html>
<?php


?>