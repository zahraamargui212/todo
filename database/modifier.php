<?php
//se connecter a la base de donnée
require_once "bd.php";

// si la session n'est pas active 
if(session_status() !== PHP_SESSION_ACTIVE) { 
    //ouvrir une session
    session_start();  
}
//verifier si utilisateur n'est pas connecter 
if(!isset($_SESSION['idUser'])){
    //redirection vers la page connexion
    header('Location: ../connexion.php');
}

$id_todo = $_GET['id_todo'];

////correction////////////
/// le id connecter peut modifier que son todo 
if (empty($_GET['id_todo'])) {
    header('Location: ../todo.php');
   
}


/////////corection//////////////*/
try {
    $requeteSQL = "SELECT * FROM todo WHERE id_todo = :id;";

    // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
    $requetePreparee = $db->prepare($requeteSQL);

    // On execute la requête préparée 
    // ici on passe en paramètre l'id du user connecté
    $requetePreparee->execute(
        [
            "id" => $id_todo
        ]
    );

    // On renvoi l'ensemble des résultats de la requête
    $todo = $requetePreparee->fetchAll();


} catch (Exception $exception) {
    echo $exception->getMessage();
    return false;
}

/////////////////////////corection///////////////////
//l'utilisateur connecter peut modifier que son todo pas les autre idUser=>stocker dans la session et id_user=>  
if ($_SESSION['idUser']!== $todo[0]['id_user']) {
   header('Location: ../todo.php');
}  
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style.css">

</head>

<body>
    <form action="" method="post">
        <div class="field-container">
            <label for="titre">Titre: <span class="required"></span></label>
            <input type="text" id="titre" name="titre" required value="<?php echo $todo[0]['titre'] ?>" />
        </div>

        <div class="field-container">
            <label for="description">Description: <span class="required"></span></label>
            <input type="text" id="description" name="description" required value='<?php echo $todo[0]['description'] ?> ' />
        </div>

        <div class="field-container">
            <label for="date">Cree_le: <span class="required"></span></label>
            <input type="date" id="date" name="cree_le" required value='<?php echo $todo[0]["cree_le"] ?>' />
        </div>

        <div class="field-container">
            <label for="date_limite">Date_limite: <span class="required"></span></label>
            <input type="date" id="date_limite" name="date_limite" required value = '<?php echo $todo[0]["date_limite"] ?>' />
        </div>

        <div class="field-container">
            <label for="status">Status: <span class="required"></span></label>
            <select name="status">
                <option value="A faire">A faire</option>
                <option value="En cours">En cours</option>
                <option value="Fait">Fait</option>
            </select>
            <!-- <input type="text" id="status" name="status" required value=<?php //echo $todo[0]["status"] 
                                                                                ?> /> -->
        </div>

        <div class="field-container">
            <label for="categorie">Categorie: <span class="required"></span></label>
            <input type="text" id="categorie" name="categorie" required value='<?php echo $todo[0]["categorie"] ?>' />
        </div>
        <div class="center"><input type="submit" name="modifier" value="Modifier"></div>


    </form>
</body>

</html>




<?php


//afficher user a modifier/////////////////////////////////



if (isset($_POST['modifier'])) {

    $titre = htmlspecialchars(trim($_POST['titre']));
    $description = htmlspecialchars(trim($_POST['description']));
    $categorie = htmlspecialchars(trim($_POST['categorie']));
    $status = htmlspecialchars(trim($_POST['status'])); // il sera aprés une option
    $date_limite = $_POST['date_limite'];
    $cree_le = $_POST['cree_le'];
    try {
        $requeteSQL = "UPDATE  todo SET titre=:titre, description =:description, cree_le =:cree_le, date_limite=:date_limite, status =:status, categorie=:categorie WHERE id_todo=:id";
        $requetePreparee = $db->prepare($requeteSQL);
        $requetePreparee->bindValue(':titre', $titre, PDO::PARAM_STR);
        $requetePreparee->bindValue(':description', $description, PDO::PARAM_STR);
        $requetePreparee->bindParam(":cree_le", $cree_le, PDO::PARAM_STR);
        $requetePreparee->bindParam(":date_limite", $date_limite, PDO::PARAM_STR);
        $requetePreparee->bindValue(':status', $status, PDO::PARAM_STR);
        $requetePreparee->bindValue(':categorie', $categorie, PDO::PARAM_STR);
        $requetePreparee->bindValue(':id', $todo[0]['id_todo'], PDO::PARAM_STR);

        $requetePreparee->execute();
        // renvoi l'id créé lors de l'insertion 
        echo "bien modifier";
        //return $requetePreparee->rowCount();

    } catch (Exception $e) {
        echo $e->getMessage();
        //return false; 
    }
    header('location: ../todo.php');
} 

































        /* if (!empty($_POST['passkey'])  && !empty($_POST['email']) && !empty($_POST['pseudo'])) {

        $pseudo = $_POST['pseudo'];
        $pseudo = htmlspecialchars($_POST['pseudo']);

        $search_html = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        $pass = $_POST['passkey'];
        $hashed_pass = password_hash($pass, PASSWORD_DEFAULT);

        $email = htmlspecialchars($_POST['email']);
        $email = trim($email);
        
        $sql ="SELECT * FROM user WHERE email=:email AND  pseudo = :pseudo";
        $reponse = $db->prepare($sql);
        $reponse->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
        //$reponse->bindValue(':password', $pass, PDO::PARAM_STR);
        $reponse->bindValue(':email', $email, PDO::PARAM_STR);
        $reponse->execute();
        $user = $reponse->fetch();
        if ($id_user) {
            // On a un utilisateur, il faut verfier le mot de pass
            password_verify($pass,$hashed_pass);
            $_SESSION['id_user'] = $user['id_user'];
            header('Location:todo.php');
        } else {
            echo "infos de connexion invalides ";
        }
        // print_r($reponse->rowCount()) ;
        // header('Location:deconnexion.php');
    }
}*/
    ////////////////////////////////////////////:::::


    /* $requeteSQL = "UPDATE todo SET titre=:titre, description=:description, cree_le=:cree_le, date_limite=:date_limite, status=:status, categorie=:categorie WHERE id_todo=:id";

    $requetePreparee = $db->prepare($requeteSQL);
    $data = [
        ':titre' => $titre,
        ':description' => $description,
        ":cree_le" => $cree_le,
        ":date_limite" => $date_limit,
        ':status' => $status,
        ':categorie' => $categorie,
    ];
    $requetePreparee->execute($data);

    header('location: todo.php');*/
