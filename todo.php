<?php
// on la démarre 
session_start();
// on récupère le fichier bd.php
require_once "database/bd.php";

// récupérer l'id de l'utilisateur connecté si elle existe
if(isset($_SESSION['idUser'])){
 


// récupérer les todos qui appartiennent à cet utilisateur

try{
    $requeteSQL = "SELECT * FROM todo WHERE id_user = :id;"; 

    // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
    $requetePreparee = $db->prepare($requeteSQL); 

    // On execute la requête préparée 
    // ici on passe en paramètre l'id du user connecté
    $requetePreparee->execute(
        [
            "id" => $_SESSION['idUser']
        ]
    ); 

    // On renvoi l'ensemble des résultats de la requête
    $resultats = $requetePreparee->fetchAll(); 
} catch(Exception $exception) {
    echo $exception->getMessage();
    return false;  
}
}

?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>todo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="style.css">
    

</head>

<body class="bg-primary ">
    <!--<a class="btn btn-danger" href="http://localhost:8000/deconnexion.php">Deconnexion</a>-->
    <div class="container ">
        <div class="row ">
            <div class="col-md-8 mx-auto mt-4 ">
                <div class="card ">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Titre</th>
                                    <th>Description</th>
                                    <th>Crée_le</th>
                                    <th>Date_limite</th>
                                    <th>Status</th>
                                    <th>Categorie</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- une ligne contient un todo -->
                               <?php foreach($resultats as $todo):  ?>
                                <tr>
                                    <td><?php echo $todo['titre'] ?></td>
                                    <td><?php echo $todo['description']?></td>
                                    <td><?php echo $todo['cree_le']?></td>
                                    <td><?php echo $todo['date_limite']?></td>
                                    <td><?php echo $todo['status']?></td>
                                    <td><?php echo $todo['categorie']?></td>
                                    <td>
                                        <a href="database/modifier.php?id_todo=<?php echo $todo['id_todo'] ?>" class="btn btn-warning btn-xs"><i class="bi bi-pencil-fill"></i></a>
                                        <a href="database/supprimer.php?id_todo=<?php echo $todo['id_todo'] ?>" class="btn btn-danger btn-xs"><i class="bi bi-trash3-fill"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach;  ?>
                                <!-- fin de la ligne -->
                            </tbody>
                        </table>
                        <div class="center">
                            <!-- <input type="submit" name="submit" value="Ajouter"> -->
                            <a href="database/ajouter.php" class="btn btn-primary">Ajouter</a>
                            <a class="btn btn-danger" href="http://localhost:8000/deconnexion.php">Deconnexion</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>

</html>

<?php


?>